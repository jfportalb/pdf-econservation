// Adapted from https://github.com/WebDevSimplified/Drag-And-Drop/blob/master/script.js

function makeElementDraggable(element) {
    element.classList.add('draggable')
    element.setAttribute('draggable', true);

    element.addEventListener('dragstart', () => {
        element.classList.add('dragging')
    })

    element.addEventListener('dragend', () => {
        element.classList.remove('dragging')
    })
}

function allowDraggingIntoContainer(container) {
    container.addEventListener('dragover', e => {
        e.preventDefault()
        const afterElement = getDragAfterElement(container, e.clientX, e.clientY)
        const draggable = document.querySelector('.dragging')
        if (afterElement == null) {
            container.appendChild(draggable)
        } else {
            container.insertBefore(draggable, afterElement)
        }
    })
}

function getDragAfterElement(container, cursorX, cursorY) {
    const draggableElements = [...container.querySelectorAll('.draggable')]

    return draggableElements.reduce((closest, child) => {
        const box = child.getBoundingClientRect()
        const offsetX = cursorX - box.left - box.width / 2
        const offsetY = cursorY - box.bottom
        if (offsetY < 0 && offsetY > closest.offsetY && offsetX < 0 &&  offsetX > closest.offsetX) {
            return { offsetX: offsetX, offsetY: offsetY, element: child }
        } else {
            return closest
        }
    }, { offsetX: Number.NEGATIVE_INFINITY, offsetY: Number.NEGATIVE_INFINITY }).element
}
