
const { PDFDocument, degrees } = PDFLib

async function createPdf(basePdfFile, images, code, id) {
    // Load base pdf into a PDFDocument object
    const pdfDoc = await PDFDocument.load(await readFileAsArrayBuffer(basePdfFile));

    // Add every uploaded image to the PDFDocument
    await [...images].forEach(async img => typeof img == "string"
        ? await drawImage(pdfDoc, img, img.split('/', 2)[1].split(';', 2)[0]) // Assuming string is dataUrl
        : await drawImageFromFile(pdfDoc, img)) // Otherwise assuming img is a File

    // Set the PDFDocument metadata
    const title = `${code}_${id}_Rx`
    pdfDoc.setTitle(title)
    pdfDoc.setAuthor("Vet Equipage")

    // Serialize the PDFDocument to bytes (a Uint8Array)
    const pdfBytes = await pdfDoc.save()

    // Trigger the browser to download the PDF document
    download(pdfBytes, `${title}.pdf`, "application/pdf");
}

const cleanInputs = _ => {
    let inputs = document.getElementsByTagName("input")
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].value = ''
    }
}

const drawImagesFromFetch = (pdfDoc, vetequipageId) =>
    fetch(`http://localhost:8080/https://app2.in/vetequipage/index_layout.php?area=img-rx&id=${vetequipageId}`)
        .then(res => res.text())
        .then(html => new DOMParser().parseFromString(html, "text/html"))
        .then(doc =>
            [...doc.getElementsByTagName("img")]
                .filter(img => img.getAttribute("src").includes("img-raio") && !img.classList.contains("lb-image"))
                .map(img => img.getAttribute("src"))
                .map(async img => {
                    const jpgImageBytes = await fetch(`http://localhost:8080/https://app2.in/vetequipage/${img}`).then((res) => res.arrayBuffer())
                    await drawImage(pdfDoc, jpgImageBytes)
                })
        ).then(r => Promise.all(r));

const drawImageFromFile = (pdfDoc, imageFile) => new Promise(async resolve => {
    const fileBuffer = await readFileAsArrayBuffer(imageFile)
    console.log(fileBuffer)
    await drawImage(pdfDoc, fileBuffer, imageFile.name.split(".").pop())
    resolve(pdfDoc);
});

const readFileAsArrayBuffer = file => new Promise(async resolve => {
    let fileReader = new FileReader();
    fileReader.readAsArrayBuffer(file);
    fileReader.onload = async _ => resolve(fileReader.result)
});

const getImageDimsScale = (imageWidth, imageHeight, pageWidth, pageHeight) => {
    let rotated = imageWidth > imageHeight;
    let w = rotated ? imageHeight : imageWidth
    let h = rotated ? imageWidth : imageHeight
    let scale = pageWidth / w;
    if (h * scale > pageHeight) scale = pageHeight / h
    return { scale, rotated }
}

const drawImage = async (pdfDoc, imageBytes, imageType) => {
    let image;
    console.log(imageType)
    switch (imageType) {
        case "png":
            image = await pdfDoc.embedPng(imageBytes);
            break;
        case "jpg":
        case "jpeg":
            console.log("JPEG")
            image = await pdfDoc.embedJpg(imageBytes);
            break;
        default:
            console.error("Failed to drawImage with type " + imageType)
            return;
    }
    let page = pdfDoc.addPage()
    let { scale, rotated } = getImageDimsScale(image.width, image.height, page.getWidth(), page.getHeight());
    imageDims = image.scale(scale)
    page.drawImage(image, {
        x: page.getWidth() / 2 + (rotated ? imageDims.height : -imageDims.width) / 2,
        y: page.getHeight() / 2 - (rotated ? imageDims.width : imageDims.height) / 2,
        width: imageDims.width,
        height: imageDims.height,
        rotate: rotated ? degrees(90) : degrees(0)
    });
}
